package com.example.appengine.demos.springboot;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.web.client.RestTemplate;



public class Client {
	private static final Logger log = LoggerFactory.getLogger(Client.class);

	public static void main(String[] args) {
		
		RestTemplate restTemplate = new RestTemplate();
		System.out.println(".........0........");
		
		// Ajouter une voiture
		HttpEntity<Car> request = new HttpEntity<>(new Car(8, "999999", "Peugeot", 12));
		restTemplate.postForObject("http://localhost:8080/car", request, Car.class);
		System.out.println(".........1........");
}
}