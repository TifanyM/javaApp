package com.example.appengine.demos.springboot;

public class Location {

	String startDate;
	String endDate;
	String rentedCar;
	String status;

	public Location() {
		super();
	}
	
	public Location(String startDate, String endDate, String rentedCar) {
		super();
		this.startDate = startDate;
		this.endDate = endDate;
		this.rentedCar = rentedCar;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getRentedCar() {
		return rentedCar;
	}
	public void setRentedCar(String rentedCar) {
		this.rentedCar = rentedCar;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
