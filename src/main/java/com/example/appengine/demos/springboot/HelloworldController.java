/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.appengine.demos.springboot;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.CompositeFilter;
import com.google.appengine.api.datastore.Query.CompositeFilterOperator;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Transaction;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.taskqueue.TaskOptions.Method;
import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;
import com.google.appengine.repackaged.com.google.common.base.StringUtil;
import java.util.logging.Logger;


@CrossOrigin(origins = "https://appangular-241002.appspot.com")
@RestController
public class HelloworldController {
	private static final Logger log = Logger.getLogger(HelloworldController.class.getName());
	/* Méthodes GET */

	/* Créer le catalogue */
	@GetMapping("/")
	public String run() {

		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

		Entity car = new Entity("Car", "AZER01");
		car.setProperty("brand", "BMW");
		car.setProperty("price", "1000");
		car.setProperty("rented", false);
		datastore.put(car);

		Entity car2 = new Entity("Car", "AZTY02");
		car2.setProperty("brand", "Fiat");
		car2.setProperty("price", "500");
		car2.setProperty("rented", false);
		datastore.put(car2);

		Entity car3 = new Entity("Car", "AWTY03");
		car3.setProperty("brand", "Ferrari");
		car3.setProperty("price", "20000");
		car3.setProperty("rented", false);
		datastore.put(car3);

		return "Run OK";
	}

	/* Afficher toutes les voitures */
	@GetMapping("/allcars")
	public void hello() {
		
		Queue queue = QueueFactory.getDefaultQueue();
		queue.add(TaskOptions.Builder.withUrl("/allcarsQueue").method(Method.GET));

		System.out.println("GET cars :");

//		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
//		Query q = new Query("Car");
//		List<Entity> results = datastore.prepare(q).asList(FetchOptions.Builder.withDefaults());
//		System.out.println(results);
//		return results;
		
	}

//	/* Afficher toutes les voitures disponibles */
//	//@GetMapping("/carsOld")
//	@RequestMapping(value = "/cars", method = RequestMethod.GET)
//	@ResponseBody
//	public List<Car> getListOfCars(){
//
////		Queue queue = QueueFactory.getDefaultQueue();
////		queue.add(TaskOptions.Builder.withUrl("/carsQueue").method(Method.GET));
////		queue.add(TaskOptions.Builder.withUrl("/error").method(Method.POST));
//		System.out.println("Called ! ");
//		
//		  
//		  UserService userService=UserServiceFactory.getUserService();
//		  User user=userService.getCurrentUser();
//		  if(user != null) {
//
//			  System.out.println("User  :"+user.toString());
//			  log.info("User  2:"+user.toString());
//		  }
//		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
//		Query q = new Query("Car");
//		q.setFilter(FilterOperator.EQUAL.of("rented",false));
//		List<Entity> results = datastore.prepare(q).asList(FetchOptions.Builder.withDefaults());
//		List<Car> resultsObj = new ArrayList();
//		for(Entity e : results) {
//			Car c = new Car();
//			
//			c.setPlateNumber(e.getKey().getName());
//			c.setBrand(e.getProperty("brand").toString());
//			c.setPrice(Integer.parseInt((String) e.getProperty("price")));
//			c.setRented((boolean) e.getProperty("rented"));
//			resultsObj.add(c);
//		}
//		
//		return resultsObj;
//	}
	
	/* Afficher toutes les voitures */
	@RequestMapping(value = "/cars", method = RequestMethod.GET)
	@ResponseBody
	public List<Car> getListOfCars(){

//		Queue queue = QueueFactory.getDefaultQueue();
//		queue.add(TaskOptions.Builder.withUrl("/carsQueue").method(Method.GET));
//		queue.add(TaskOptions.Builder.withUrl("/error").method(Method.POST));
		System.out.println("Called ! ");
		
		  
		
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		Query q = new Query("Car");
		//q.setFilter(FilterOperator.EQUAL.of("rented",false));
		List<Entity> results = datastore.prepare(q).asList(FetchOptions.Builder.withDefaults());
		List<Car> resultsObj = new ArrayList();
		for(Entity e : results) {
			Car c = new Car();
			
			c.setPlateNumber(e.getKey().getName());
			c.setBrand(e.getProperty("brand").toString());
			c.setPrice(Integer.parseInt((String) e.getProperty("price")));
			c.setRented((boolean) e.getProperty("rented"));
			resultsObj.add(c);
		}
		
		return resultsObj;
	}
	
	/* Afficher une voiture */
	//@GetMapping("/cars/{plateNumber}")
	@RequestMapping(value = "/cars/{plateNumber}", method = RequestMethod.GET)
	@ResponseBody
	public Car getCar(@PathVariable("plateNumber") String plateNumber) {
		System.out.println("IN GETCAR ! "+plateNumber);
		log.info("IN GETCAR ! "+plateNumber);
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		Car c = new Car();
		try {
			Key k = KeyFactory.createKey("Car", plateNumber);
			Entity e = datastore.get(k);
			c.setPlateNumber(e.getKey().getName());
			c.setBrand(e.getProperty("brand").toString());
			c.setPrice(Integer.parseInt((String) e.getProperty("price")));
			c.setRented((boolean) e.getProperty("rented"));
			
		} catch (EntityNotFoundException e) {
			e.printStackTrace();
			System.out.println("Cette voiture n'existe pas.");
		}
		return c;
	}

	/* Méthodes POST */

	/* Ajouter une voiture */
	//@PostMapping("/car")
	@RequestMapping(value = "/car", method = RequestMethod.POST)
	public void addCar(@RequestBody Car car) throws EntityNotFoundException {

		System.out.println(car);
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

		boolean exists = false;
		try {
			Key k = KeyFactory.createKey("Car", car.getPlateNumber());
			datastore.get(k);
			exists = true;
		}catch (EntityNotFoundException e) {
			e.printStackTrace();
			System.out.println("OK cette voiture n'existe pas.");
		}

		if (!exists) {
			Entity entity = new Entity("Car", car.getPlateNumber());
			entity.setProperty("brand", car.getBrand());
			entity.setProperty("price", Integer.toString(car.getPrice()));
			entity.setProperty("rented", false);
			datastore.put(entity);
		}
	}

	/* Méthodes PUT */
	
	/* Louer/rendre une voiture */
	@PutMapping ("/rent/{plateNumber}")
	public void rentAndGetBack(
			@PathVariable("plateNumber") String plateNumber,
			@RequestParam("rent") boolean rent//,
			//@RequestBody Location location
			) throws Exception{
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		System.out.println("Called RENT! ");
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		try {
			Key k = KeyFactory.createKey("Car", plateNumber);
			Entity car = datastore.get(k);
			if (rent && car.getProperty("rented").equals(false)) {
				Entity newLoc = new Entity("Location");
				Date date = new Date();
				newLoc.setProperty("startDate", dateFormat.format(date));
				//newLoc.setProperty("startDate", location.getStartDate());
				newLoc.setProperty("rentedCar", plateNumber);
				newLoc.setProperty("status", LocationStatus.LIVE.toString());
				datastore.put(newLoc);
				car.setProperty("rented", true);
			}
			else if (rent && car.getProperty("rented").equals(true)){
				System.out.println("Voiture déja louée !");
			}
			else if(!rent && car.getProperty("rented").equals(true)) {
				Query q = new Query("Location");
				CompositeFilter filter = CompositeFilterOperator.and(FilterOperator.EQUAL.of("rentedCar",plateNumber), FilterOperator.EQUAL.of("status", LocationStatus.LIVE.toString()));
				q.setFilter(filter);
				List<Entity> results = datastore.prepare(q).asList(FetchOptions.Builder.withDefaults());
				Entity loc = results.get(0);
				Date date = new Date();
				loc.setProperty("endDate", dateFormat.format(date));
				//loc.setProperty("endDate", location.getEndDate());
				loc.setProperty("status", LocationStatus.OVER.toString());
				car.setProperty("rented", false);
				datastore.put(loc);
				
			}
			else {
				System.out.println("Cette voiture n'est pas actuellement louée !");
			}
			datastore.put(car);
		} catch (EntityNotFoundException e) {
			e.printStackTrace();
			System.out.println("Cette voiture n'existe pas.");
		}
	}
}
