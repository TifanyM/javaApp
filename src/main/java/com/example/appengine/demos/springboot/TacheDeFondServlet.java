package com.example.appengine.demos.springboot;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TacheDeFondServlet {
	static int compteur = 0;
	
	@PostMapping("/error")
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		if(compteur < 3){

			resp.sendError(HttpServletResponse.SC_FORBIDDEN);
			compteur++;
			System.out.println("error");
		}
	}
}