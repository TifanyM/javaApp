package com.example.appengine.demos.springboot;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.CompositeFilter;
import com.google.appengine.api.datastore.Query.CompositeFilterOperator;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.taskqueue.TaskOptions.Method;

@RestController
public class ServiceQueue {

	/* Afficher toutes les voitures disponibles */
	@GetMapping("/carsQueue")
	public List<Car> getListOfCars(){
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		Query q = new Query("Car");
		List<Entity> results = datastore.prepare(q).asList(FetchOptions.Builder.withDefaults());
		List<Car> resultsObj = new ArrayList();
		for(Entity e : results) {
			Car c = new Car();
			
			c.setPlateNumber(e.getKey().getName());
			c.setBrand(e.getProperty("brand").toString());
			c.setPrice(Integer.parseInt((String) e.getProperty("price")));
			c.setRented((boolean) e.getProperty("rented"));
			resultsObj.add(c);
		}
		
		return resultsObj;
	}
//	/* Afficher toutes les voitures */
//	@GetMapping("/allcarsQueue")
//	public List<Entity> hello() {
//
//
//
//		System.out.println("GET cars :");
//
//		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
//
//		Query q = new Query("Car");
//		List<Entity> results = datastore.prepare(q).asList(FetchOptions.Builder.withDefaults());
//
//		System.out.println(results);
//
//		return results;
//	}

//	/* Afficher toutes les voitures disponibles */
//	@GetMapping("/carsQueue")
//	public List<Entity> getListOfCars(){
//
//		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
//		Query q = new Query("Car");
//		q.setFilter(FilterOperator.EQUAL.of("rented",false));
//		List<Entity> results = datastore.prepare(q).asList(FetchOptions.Builder.withDefaults());
//
//		return results;
//	}
	
	/* Méthodes POST */
//
//	/* Ajouter une voiture */
//	@PostMapping("/car")
//	public void addCar(@RequestBody Car car) throws EntityNotFoundException {
//
//		System.out.println(car);
//
////		Queue queue = QueueFactory.getDefaultQueue();
////		queue.add(TaskOptions.Builder.withUrl("/rent/{plateNumber}").method(Method.PUT).param("plateNumber", plateNumber));
////		
//		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
//
//		boolean exists = false;
//		try {
//			Key k = KeyFactory.createKey("Car", car.getPlateNumber());
//			datastore.get(k);
//			exists = true;
//		}catch (EntityNotFoundException e) {
//			e.printStackTrace();
//			System.out.println("OK cette voiture n'existe pas.");
//		}
//
//		if (!exists) {
//			Entity entity = new Entity("Car", car.getPlateNumber());
//			entity.setProperty("brand", car.getBrand());
//			entity.setProperty("price", car.getPrice());
//			entity.setProperty("rented", false);
//			datastore.put(entity);
//		}
//	}
//	
//	/* Méthodes PUT */
//
//	/* Louer/rendre une voiture */
//	@PutMapping ("/rent/{plateNumber}")
//	public void rentAndGetBack(
//			@PathVariable("plateNumber") String plateNumber,
//			@RequestParam("rent") boolean rent,
//			@RequestBody Location location
//			) throws Exception{
//		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
//
//		try {
//			Key k = KeyFactory.createKey("Car", plateNumber);
//			Entity car = datastore.get(k);
//			if (rent && car.getProperty("rented").equals(false)) {
//				Entity newLoc = new Entity("Location");
//				newLoc.setProperty("startDate", location.getStartDate());
//				newLoc.setProperty("rentedCar", plateNumber);
//				newLoc.setProperty("status", LocationStatus.LIVE.toString());
//				datastore.put(newLoc);
//				car.setProperty("rented", true);
//			}
//			else if (rent && car.getProperty("rented").equals(true)){
//				System.out.println("Voiture déja louée !");
//			}
//			else if(!rent && car.getProperty("rented").equals(true)) {
//				Query q = new Query("Location");
//				CompositeFilter filter = CompositeFilterOperator.and(FilterOperator.EQUAL.of("rentedCar",plateNumber), FilterOperator.EQUAL.of("status", LocationStatus.LIVE.toString()));
//				q.setFilter(filter);
//				List<Entity> results = datastore.prepare(q).asList(FetchOptions.Builder.withDefaults());
//				Entity loc = results.get(0);
//				loc.setProperty("endDate", location.getEndDate());
//				loc.setProperty("status", LocationStatus.OVER.toString());
//				car.setProperty("rented", false);
//				datastore.put(loc);
//
//			}
//			else {
//				System.out.println("Cette voiture n'est pas actuellement louée !");
//			}
//			datastore.put(car);
//		} catch (EntityNotFoundException e) {
//			e.printStackTrace();
//			System.out.println("Cette voiture n'existe pas.");
//		}
//	}
}
