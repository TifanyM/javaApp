package com.example.appengine.demos.springboot;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;

import com.google.appengine.api.NamespaceManager;

@WebFilter(
		urlPatterns = "/withFilter/*"
		)
public class NamespaceFilter implements Filter{

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

		if(NamespaceManager.get().equals("")) {

			NamespaceManager.getGoogleAppsNamespace();
			String nameSpace = request.getParameter("nameSpace");
			System.out.println("in doFilter namespace : "+ nameSpace);
			String defaultNameSpace = "TIF";
			NamespaceManager.set(defaultNameSpace);
			

		}
		chain.doFilter(request, response);
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub

	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}
}