package com.example.appengine.demos.springboot;


public class Car {
	
	private int id;
	private String plateNumber;
	private String brand;
	private int price;
	private boolean rented;
	
	public Car() {
		super();
	}
	
	public Car(int id, String plateNumber, String brand, int price) {
		super();
		this.id = id;
		this.plateNumber = plateNumber;
		this.brand = brand;
		this.price = price;
		this.rented = false;
	}
	
	public Car(String plateNumber, String brand, int price, boolean isrented) {
		super();
		this.plateNumber = plateNumber;
		this.brand = brand;
		this.price = price;
		this.rented = isrented;
	}
	
	public String getPlateNumber() {
		return plateNumber;
	}
	
	public void setPlateNumber(String plateNumber) {
		this.plateNumber = plateNumber;
	}
	
	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public int getPrice() {
		return price;
	}
	
	public void setPrice(int price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return "Car [plateNumber=" + plateNumber + ", brand=" + brand + ", price=" + price + ", rented=" + rented +"]";
	}

	public boolean isRented() {
		return rented;
	}

	public void setRented(boolean rented) {
		this.rented = rented;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

}